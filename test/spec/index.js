define(function (require) {
    'use strict';

    return {
        specs: [
            'test/spec/collection',
            'test/spec/model',
            'test/spec/helper/sync'
        ]
    };
});
