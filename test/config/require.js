require.config({
    'config': {
        'i18n': {
            'locale': 'nl-nl'
        },
    },
    'paths': {
        'jquery': 'node_modules/jquery/dist/jquery.min',
        'jasmine.mock-ajax': 'node_modules/jasmine-ajax/lib/mock-ajax',
        'backbone': 'node_modules/backbone/backbone',
        'backbone.paginator': 'node_modules/backbone.paginator/lib/backbone.paginator',
        'underscore': 'node_modules/underscore/underscore',
        'uri': 'node_modules/URIjs/src/URI',
        'punycode': 'node_modules/URIjs/src/punycode',
        'IPv6': 'node_modules/URIjs/src/IPv6',
        'SecondLevelDomains': 'node_modules/URIjs/src/SecondLevelDomains'
        // 'underscore.string': 'node_modules/underscore.string/lib/underscore.string',
        // 'lodash': 'node_modules/lodash/dist/lodash',
        // 'marionette': 'node_modules/backbone.marionette/lib/core/backbone.marionette',
        // 'backbone.wreqr': 'node_modules/backbone.wreqr/lib/backbone.wreqr',
        // 'backbone.babysitter': 'node_modules/backbone.babysitter/lib/backbone.babysitter',
        // 'backbone.validation': 'node_modules/backbone-validation/dist/backbone-validation',
        // 'backbone.virtualCollection': 'node_modules/backbone-virtual-collection/src/backbone.virtual-collection',
        // 'crux': 'node_modules/backbone-crux/src',        
        // 'isSimilar': 'node_modules/issimilar.js/isSimilar',
        // 'textRequire': 'node_modules/text/text',
        // 'text': 'text',
        // 'i18n': 'node_modules/i18n/i18n',
        // /* The extra slash at the end is important: it prevents bogus
        //  * inclusions of jqueryui.js in the same directory.
        //  */
        // 'jquery.serializeObject': 'node_modules/jquery-misc/jquery.ba-serializeobject',
        // 'jquery.ajaxLoadingBar': 'node_modules/jquery.ajaxLoadingBar/jquery.ajaxLoadingBar',
        // 'jquery.transit': 'node_modules/jquery.transit/jquery.transit',
        // 'jquery.ui': 'node_modules/jquery-ui/ui',
        // // 'query-ui/core,jquery-ui/widget,jquery-ui/position': 'node_modules/jquery-ui/ui',
        // 'jquery.cookie': 'node_modules/jquery.cookie/jquery.cookie',
        // 'jquery.form': './node_modules/jquery-form/jquery.form',
        // 'jquery.multiselect': './../assets/js/jquery.multiselect',
        // 'jquery.fileDownload': './jquery.fileDownload',
        // 'jquery.ui.contextMenu': 'node_modules/ui-contextmenu/jquery.ui-contextmenu',
        // /**
        //  * selectize requires sifter and microplugin.
        //  */
        // 'jquery.selectize': 'node_modules/selectize/dist/js/selectize',
        // 'x-no-autocomplete': 'node_modules/x-no-autocomplete/x-no-autocomplete',
        // 'daterangepicker': 'node_modules/bootstrap-daterangepicker/daterangepicker',
        // 'sifter': 'node_modules/sifter/sifter',
        // 'microplugin': 'node_modules/microplugin/src/microplugin',
        // 'moment': 'node_modules/moment/moment',
        // 'moment.durationFormat': 'node_modules/moment-duration-format/lib/moment-duration-format',
        // 'marionette-crux-paginator': 'node_modules/marionette-crux-paginator/marionette-crux-paginator',
        // 'marionette-notifications': 'node_modules/marionette-notifications',
        // 'phone': 'node_modules/simple-phone-api/dist/phone',
        // 'intro': 'node_modules/intro.js/intro',
        // 'd3': 'node_modules/d3/d3',
        // 'nvd3': 'node_modules/nvd3/nv.d3'
    },
    'shim': {
        // 'backbone': {
        //     'deps': ['underscore', 'jquery'],
        //     'exports': 'Backbone'
        // },
        // 'backbone.paginator': {
        //     'deps': ['backbone'],
        //     'exports': 'Backbone.Paginator'
        // },
        // 'backbone.validation': {
        //     'deps': ['backbone'],
        // },
        // 'daterangepicker': {
        //     'deps': ['jquery', 'moment']
        // },
        // 'backbone.cocktail': {
        //     'exports': 'Cocktail'
        // },
        // 'underscore': {
        //     'exports': '_'
        // },
        // 'jquery.imagesLoaded': {
        //     'deps': ['jquery']
        // },
        // 'jquery.serializeObject': {
        //     'deps': ['jquery']
        // },
        // 'jquery.transit': {
        //     'deps': ['jquery']
        // },
        // 'jquery.cookie': {
        //     'deps': ['jquery']
        // },
        // 'jquery.selectize': {
        //     'deps': ['jquery']
        // },
        // 'jquery.multiselect': {
        //     'deps': ['jquery.ui/widget']
        // },
        // 'moment.durationFormat': {
        //     'deps': ['underscore', 'lodash', 'moment'],
        // },
        // 'isSimilar': {
        //     'exports': 'isSimilar',
        // },
        // 'nvd3': {
        //     'exports': 'nv',
        //     'deps': ['d3']
        // },
        // 'collection/project': {
        //     'deps': ['collection/specification']
        // },
        // 'model/project': {
        //     'deps': ['collection/oem']
        // },
        // 'backbone.virtualCollection': {
        //     'deps': ['backbone'],
        //     'exports': 'VirtualCollection',
        // },
        // 'marionette-crux-paginator': {
        //     'deps': ['jquery', 'marionette', 'underscore']
        // },
        // 'config/app': {
        //     'deps': ['underscore']
        // },
    }
});
